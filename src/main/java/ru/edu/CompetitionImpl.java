package ru.edu;

import ru.edu.model.Athlete;
import ru.edu.model.CountryParticipant;
import ru.edu.model.Participant;


import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CompetitionImpl implements Competition {
    /**
     * Порядковый номер участников.
     */
    private long id = 1;
    /**
     * Множество участников.
     */
    private Set<Athlete> athletesList = new HashSet<>();
    /**
     * Карта участников.
     */
    private Map<Long, ParticipantImpl> participantsMap = new HashMap<>();
    /**
     * Карта стран - участниц.
     */
    private Map<String, CountryParticipantImpl> countryMap = new HashMap<>();
    /**
     * Регистрация участника.
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    @Override
    public Participant register(final Athlete participant) {
        if (!athletesList.add(participant)) {
            throw new IllegalArgumentException(
                    "This athlete is already registered");
        } else {
            String countryName = participant
                    .getCountry()
                    .toUpperCase();
            ParticipantImpl internalParticipant =
                    new ParticipantImpl(participant, id);

            participantsMap.put(id, internalParticipant);

            if (!countryMap.containsKey(countryName)) {
                countryMap.put(
                        countryName,
                        new CountryParticipantImpl(countryName)
                );
            }
            countryMap.get(countryName).addParticipants(internalParticipant);
        }
        return new ParticipantImpl(participant, id++);
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param participantId    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(final long participantId, final long score) {
        if (!participantsMap.containsKey(participantId)) {
            throw new IllegalArgumentException("id does not exist");
        }
        ParticipantImpl participant = participantsMap.get(participantId);
        participant.updateScore(score);
    }

    /**
     * Обновление счета участника по его объекту Participant
     * Требуется константное время выполнения.
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    @Override
    public void updateScore(final Participant participant, final long score) {
        updateScore(participant.getId(), score);
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<Participant> getResults() {
        List<Participant> resultScore =
                new LinkedList<>(participantsMap.values());
        resultScore.sort(Comparator
                .comparingLong(Participant::getScore)
                .reversed());
        return resultScore;
    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<CountryParticipant> getParticipantsCountries() {
        List<CountryParticipant> countryList =
                new LinkedList<>(countryMap.values());
        countryList.sort(Comparator
                .comparingLong(CountryParticipant::getScore)
                .reversed());
        return countryList;
    }

    private static class ParticipantImpl implements Participant {
        /**
         * Порядковый номер участника.
         */
        private Long id;
        /**
         * Счет участника.
         */
        private long score;
        /**
         * Объект содержащий сведения об атлете.
         */
        private Athlete athlete;
        /**
         * Конструктор объекта участника.
         * @param participant атлет
         * @param participantId номер участника
         */
        ParticipantImpl(final Athlete participant, final Long participantId) {
            athlete = participant;
            id = participantId;
        }
        /**
         * Получение информации о регистрационном номере.
         *
         * @return регистрационный номер
         */
        @Override
        public Long getId() {
            return id;
        }

        /**
         * Информация о спортсмене.
         *
         * @return объект спортсмена
         */
        @Override
        public Athlete getAthlete() {
            return athlete;
        }

        /**
         * Счет участника.
         *
         * @return счет
         */
        @Override
        public long getScore() {
            return score;
        }
        /**
         * Изменение счета участника.
         *
         * @param value - дельта счета
         */
        public void updateScore(final long value) {
            score += value;
        }
    }

    private static class CountryParticipantImpl implements CountryParticipant {
        /**
         * Название страны - участницы.
         */
        private String name;
        /**
         * Список участников от страны.
         */
        private List<Participant> participants = new LinkedList<>();
        /**
         * Конструктор объекта страны - участницы.
         *
         * @param countryName имя страны
         */
        CountryParticipantImpl(final String countryName) {
            name = countryName;
        }
        /**
         * Добавление участника в список атлетов страны.
         *
         * @param participant - участник от страны.
         */
        public void addParticipants(final Participant participant) {
            participants.add(participant);
        }
        /**
         * Название страны.
         *
         * @return название
         */
        @Override
        public String getName() {
            return name;
        }

        /**
         * Список участников от страны.
         *
         * @return список участников
         */
        @Override
        public List<Participant> getParticipants() {
            return participants;
        }

        /**
         * Счет страны.
         *
         * @return счет
         */
        @Override
        public long getScore() {
            long score = 0;
            for (Participant participant: participants) {
                score += participant.getScore();
            }
            return score;
        }
    }
}

package ru.edu.model;

import java.util.Objects;

public class AthleteImpl implements Athlete {
    /**
     * Имя.
     */
    private String firstName;
    /**
     * Фамилия.
     */
    private String lastName;
    /**
     * Страна.
     */
    private String country;
    /**
     * Конструктор объекта атлета.
     *
     * @param athleteCountry  - страна
     * @param athleteFirstName - имя
     * @param athleteLastName - фамилия
     */
    public AthleteImpl(
            final String athleteFirstName,
            final String athleteLastName,
            final String athleteCountry
    ) {
        firstName = athleteFirstName;
        lastName = athleteLastName;
        country = athleteCountry;
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }
    /**
     * @return равенство
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AthleteImpl athlete = (AthleteImpl) o;
        return
                Objects.equals(
                        firstName.toLowerCase(),
                        athlete.firstName.toLowerCase()
                ) && Objects.equals(
                        lastName.toLowerCase(),
                        athlete.lastName.toLowerCase()
                ) && Objects.equals(
                        country.toLowerCase(),
                        athlete.country.toLowerCase()
                );
    }
    /**
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, country);
    }
    /**
     * @return toString
     */
    @Override
    public String toString() {
        return "AthleteImpl{"
                + "firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", country='" + country + '\''
                + '}';
    }
}

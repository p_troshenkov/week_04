package ru.edu;

import ru.edu.model.Athlete;
import ru.edu.model.CountryParticipant;
import ru.edu.model.Participant;

import java.util.List;

/**
 * Класс реализующий логику проведения соревнования.
 */
public interface Competition {

    /**
     * Регистрация участника.
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    Participant register(Athlete participant);

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    void updateScore(long id, long score);

    /**
     * Обновление счета участника по его объекту Participant
     * Требуется константное время выполнения.
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    void updateScore(Participant participant, long score);

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    List<Participant> getResults();

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    List<CountryParticipant> getParticipantsCountries();

}

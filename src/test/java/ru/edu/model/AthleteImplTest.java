package ru.edu.model;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class AthleteImplTest {

    Athlete athlete1 = new AthleteImpl("Alex", "Ivanov", "Russia");
    Athlete athlete2 = new AthleteImpl("Ivan", "Petrov", "France");
    Athlete athlete3 = new AthleteImpl("Alex", "Ivanov", "Russia");


    @Test
    public void getFirstName() {
        assertEquals("Alex", athlete1.getFirstName());
        assertEquals("Ivan", athlete2.getFirstName());
    }

    @Test
    public void getLastName() {
        assertEquals("Ivanov", athlete1.getLastName());
        assertEquals("Petrov", athlete2.getLastName());
    }

    @Test
    public void getCountry() {
        assertEquals("Russia", athlete1.getCountry());
        assertEquals("France", athlete2.getCountry());
    }

    @Test
    public void testEquals() {
        assertTrue(athlete1.equals(athlete1));
        assertTrue(athlete1.equals(athlete3));

        assertFalse(athlete1.equals(athlete2));
        assertFalse(athlete1.equals(null));
        assertFalse(athlete1.equals("athlete"));
    }

    @Test
    public void testHashCode() {
        assertTrue(athlete1.hashCode() == athlete3.hashCode());

        assertFalse(athlete1.hashCode() == athlete2.hashCode());
    }

    @Test
    public void testToString() {
        assertEquals("AthleteImpl{firstName='Alex', lastName='Ivanov', country='Russia'}", athlete1.toString());

    }
}
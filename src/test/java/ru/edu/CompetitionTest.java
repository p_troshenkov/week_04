package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.model.AthleteImpl;
import ru.edu.model.Participant;

import static org.junit.Assert.*;


public class CompetitionTest {
    Competition competition = new CompetitionImpl();

    AthleteImpl athlete1 = new AthleteImpl("Ivan", "Petrov", "Russia");
    AthleteImpl athlete2 = new AthleteImpl("Ivan", "Petrov", "Russia");
    AthleteImpl athlete3 = new AthleteImpl("John", "Smith", "USA");
    AthleteImpl athlete4 = new AthleteImpl("Toyama", "Tokanava", "Japan");
    AthleteImpl athlete5 = new AthleteImpl("Petr", "Ivanov", "Russia");
    AthleteImpl athlete6 = new AthleteImpl("Andrei", "Sidorov", "Russia");
    AthleteImpl athlete7 = new AthleteImpl("Michael", "Norton", "USA");
    AthleteImpl athlete8 = new AthleteImpl("Troy", "Itkis", "USA");

    Participant participant1 = competition.register(athlete1);
    Participant participant3 = competition.register(athlete3);
    Participant participant4 = competition.register(athlete4);
    Participant participant5 = competition.register(athlete5);
    Participant participant6 = competition.register(athlete6);
    Participant participant7 = competition.register(athlete7);



    @Test (expected = IllegalArgumentException.class)
    public void registerException() {
        competition.register(athlete2);
    }

    @Test
    public void register() {
        assertEquals(athlete8, competition.register(athlete8).getAthlete());
    }

    @Test (expected = IllegalArgumentException.class)
    public void updateScoreException() {
        competition.updateScore(10, 10);
    }

    @Test
    public void updateScore() {
        competition.updateScore(1, 10);
        assertEquals(competition.getResults().get(0).getScore(), 10);
        competition.updateScore(1, 20);
        assertEquals(competition.getResults().get(0).getScore(), 30);
        competition.updateScore(1, -15);
        assertEquals(competition.getResults().get(0).getScore(), 15);
    }

    @Test
    public void testUpdateScore() {
        competition.updateScore(participant1, 10);
        assertEquals(competition.getResults().get(0).getScore(), 10);
        competition.updateScore(participant1, 20);
        assertEquals(competition.getResults().get(0).getScore(), 30);
        competition.updateScore(participant1, -15);
        assertEquals(competition.getResults().get(0).getScore(), 15);
    }

    @Test
    public void getResults() {
        competition.updateScore(1, 10);
        assertEquals(competition.getResults().get(0).getScore(), 10);

        competition.updateScore(2, 100);
        assertEquals(competition.getResults().get(0).getScore(), 100);
        assertEquals(competition.getResults().get(1).getScore(), 10);

        competition.updateScore(participant4, 50);
        assertEquals(competition.getResults().get(0).getScore(), 100);
        assertEquals(competition.getResults().get(1).getScore(), 50);
        assertEquals(competition.getResults().get(2).getScore(), 10);

    }

    @Test
    public void getParticipantsCountries() {
        competition.updateScore(1, 10); // + 10 Russia
        assertEquals(competition.getParticipantsCountries().get(0).getScore(), 10);

        competition.updateScore(4, 20); // + 20 Russia
        competition.updateScore(5, 30); // + 30 Russia
        assertEquals(competition.getParticipantsCountries().get(0).getScore(), 60);

        competition.updateScore(2, 50); // + 50 USA
        competition.updateScore(3, 100); // + 100 Japan
        competition.updateScore(6, 60); // + 60 USA
        assertEquals(competition.getParticipantsCountries().get(0).getScore(), 110);
        assertEquals(competition.getParticipantsCountries().get(1).getScore(), 100);
        assertEquals(competition.getParticipantsCountries().get(2).getScore(), 60);

        assertEquals(competition.getParticipantsCountries().get(0).getName(), "USA");
        assertEquals(competition.getParticipantsCountries().get(1).getName(), "JAPAN");
        assertEquals(competition.getParticipantsCountries().get(2).getName(), "RUSSIA");


    }
}